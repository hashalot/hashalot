#!/bin/sh

#
# hacked by <hvr@gnu.org>
# re-hacked by <sluskyb@stwing.org>
#

testcases() {
    ALGO="$1"
    shift

    echo -n "checking $ALGO..."

    while [ $# -gt 0 ]; do
	TEXT=$1
	HASH=$2
	shift 2

	_RESULT=`echo "$TEXT" | ./hashalot -x $ALGO 2> /dev/null`
	if [ "$_RESULT" != "$HASH" ]; then
	    echo FAILED
	    return
	fi
    done
    echo OK
}

testcases sha256 \
"abc" "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad" \
"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" "248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1"

testcases sha384 \
"abc" "cb00753f45a35e8bb5a03d699ac65007272c32ab0eded1631a8b605a43ff5bed8086072ba1e7cc2358baeca134c825a7" \
"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" "3391fdddfc8dc7393707a65b1b4709397cf8b1d162af05abfe8f450de5f36bc6b0455a8520bc4e6f5fe95b1fe3c8452b" \
"abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu" "09330c33f71147e83d192fc782cd1b4753111b173b3b05d22fa08086e3b0f712fcc7c71a557e2db966c3e9fa91746039" \
"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" "3d208973ab3508dbbd7e2c2862ba290ad3010e4978c198dc4d8fd014e582823a89e16f9b2a7bbc1ac938e2d199e8bea4"

testcases sha512 \
"abc" "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f" \
"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" "204a8fc6dda82f0a0ced7beb8e08a41657c16ef468b228a8279be331a703c33596fd15c13b1b07f9aa1d3bea57789ca031ad85c7a71dd70354ec631238ca3445" \
"abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu" "8e959b75dae313da8cf4f72814fc143f8f7779c6eb9f7fa17299aeadb6889018501d289e4900f7e4331b99dec4b5433ac7d329eeb6dd26545e96e55b874be909" \
"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" "930d0cefcb30ff1133b6898121f1cf3d27578afcafe8677c5257cf069911f75d8f5831b56ebfda67b278e66dff8b84fe2b2870f742a580d8edb41987232850c9"

testcases ripemd160 \
"" "9c1185a5c5e9fc54612808977ee8f548b2258d31" \
"a" "0bdc9d2d256b3ee9daae347be6f4dc835a467ffe" \
"abc" "8eb208f7e05d987a9b044a8e98c6b087f15a0bfc" \
"message digest" "5d0689ef49d2fae572b881b123a85ffa21595f36" \
"abcdefghijklmnopqrstuvwxyz" "f71c27109c692c1b56bbdceb5b9d2865b3708dbc" \
"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" "12a053384a9c0c88e405a06c27dcf49ada62eb2b" \
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789" "b0e20b6e3116640286ed3a87a5713079b21f5189" \
"12345678901234567890123456789012345678901234567890123456789012345678901234567890" "9b752e45573d4b39f4dbd3323cab82bf63326bfb"

